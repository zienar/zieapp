import { NgModule, APP_INITIALIZER } from '@angular/core';
import {/* HttpClient, */ HttpClientModule} from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import {IonicStorageModule} from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { File } from '@ionic-native/file/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { IonicConfig } from '@ionic/core';
import {ApiService} from './services/api.service';
import {AuthService} from './services/auth.service';
import {LoggerService} from './services/logger.service';
import {DataService} from './services/data.service';

const zconfig: IonicConfig = {
 mode: 'ios',
 swipeBackEnabled: false,
 loadingSpinner: 'crescent',
};

const sconfig = {
name: '_zienar',
storeName: 'dbkvp',
driverOrder: ['sqlite', 'indexeddb', 'websql', 'localstorage']
};

export function providerFactory(data: DataService) {
  return () => {
    data.InitData();
  };
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, HttpClientModule, IonicModule.forRoot(zconfig), IonicStorageModule.forRoot(sconfig), AppRoutingModule],
  providers: [
    {provide: APP_INITIALIZER, useFactory: providerFactory, deps: [DataService], multi: true},
    StatusBar,
    SplashScreen,
    File,
    ApiService,
    AuthService,
    LoggerService,
    DataService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

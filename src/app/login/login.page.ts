import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  userid: string;
  pword: string;

inType = 'password';
inIcon = 'eye';


  constructor(private auth: AuthService, private navCtrl: NavController) {
  }

  ngOnInit() {
  }

  buttonclick() {
    this.inType = this.inType === 'text' ? 'password' : 'text';
    this.inIcon = this.inIcon === 'eye-off' ? 'eye' : 'eye-off';
   // console.log('tapped');
     }

     submit() {
      this.auth.loginAccount(this.userid, this.pword).then(() => {
        this.navCtrl.navigateRoot('/home');
      });
      // .catch();
     }

}

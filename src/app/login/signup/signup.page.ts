import { Component, OnInit } from '@angular/core';
import {parsePhoneNumberFromString} from 'libphonenumber-js/mobile';
import { AuthService } from 'src/app/services/auth.service';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  userid: string;
  Securitycode: string;
  pword: string;

inType = 'password';
inIcon = 'eye';

  constructor(private auth: AuthService, private navCtrl: NavController) {   }

  ngOnInit() {
  }

  buttonclick() {
   this.inType = this.inType === 'text' ? 'password' : 'text';
   this.inIcon = this.inIcon === 'eye-off' ? 'eye' : 'eye-off';
  // console.log('tapped');
    }


    IsValid(): boolean {
   let valid = true;

   const phoneNumber = parsePhoneNumberFromString(this.userid);

    if (this.userid.length === 0 || (!this.validateEmail(this.userid) && !phoneNumber )) {
      valid = false;
    }

    if (this.pword.length === 0) {
      valid = false;
    }

    return valid;
    }

   submit() {
   console.log('submit happening');
   // if (this.IsValid()) {
     // console.log('valid');
     this.auth.createAccount(this.userid, this.pword).then(() => {
       this.navCtrl.navigateRoot('/home');
     });
   // }

   }

  // very basic email check
   validateEmail(email: string) {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

    takepicture() {}

}

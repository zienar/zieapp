import { Injectable } from '@angular/core';
import { File } from '@ionic-native/file/ngx';
import * as moment from 'moment';
import {Platform} from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  DirName: string;
  logPath: string;
  logFileName: string;
  logfilePath: string;
  bProdEnv: boolean;

    constructor(private file: File, public platform: Platform) {
      const self = this;
        this.platform.ready (). then ((readySource) => {
          self.DirName = 'Logs';
          self.bProdEnv = true;
          self.logPath = self.file.dataDirectory + self.DirName + '/';
          const iCount = new Date();
          const date = iCount.toISOString().replace(/:/g, '-');
            self.logFileName = 'zienar_' + date + '.log';
          self.logfilePath = self.logPath + self.logFileName;
          if (self.bProdEnv) {
            self.initLogFile();
          }
        });
  }

  async initLogFile() {
    try {

      await this.file.createDir(this.file.dataDirectory, this.DirName, true);

      await this.file.createFile(this.logPath, this.logFileName, true);
      await this.writeLogFile(`Date\t\tText\t\tErrors\t\tWarnings`);
    } catch (error) {
      console.log(error);
    }
  }

  async logInfo(aMsg) {
    const sMsg = this.getLogMessage(aMsg, false, false);
    await this.writeLogFile(sMsg);
  }

  async logError(aMsg) {
    const sMsg = this.getLogMessage(aMsg, true, false);
    await this.writeLogFile(sMsg);
  }

  async logWarning(aMsg) {
    const sMsg = this.getLogMessage(aMsg, false, true);
    await this.writeLogFile(sMsg);
  }

  async logDebug(aMsg: string, sType: string) {
      switch (sType) {
          case 'info':
          this.logInfo(aMsg);
          break;
          case 'error':
          this.logError(aMsg);
          break;
          case 'warning':
          this.logWarning(aMsg);
          break;
          default:
          console.log('wrong debug type, ignoring message:\n' + aMsg);
          break;
        }
        console.log(aMsg);
  }

  getLogMessage(aMsg, bError, bWarning) {
    const iError = bError ? 1 : 0;
    const iWarning = bWarning ? 1 : 0;
    return `${moment().format('YYYY.MM.DD HH:mm:ss')}\t${aMsg}\t${iError}\t${iWarning}`;
  }

  async writeLogFile(aMsg) {
    await this.file.writeFile(this.logPath, this.logFileName, aMsg + '\r', { 'replace': false, 'append': true });
  }
  async writeExistingLogFile(aMsg) {
    await this.file.writeExistingFile(this.logPath, this.logFileName, aMsg);
  }
}

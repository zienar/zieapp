import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { DataService } from './data.service';



@Injectable({
  providedIn: 'root'
})
export class ApiService {

private _url: string;
private _reqOPts: any;

  constructor(private http: HttpClient, private data: DataService) {
    this._url = 'http://192.168.0.108:600';

    this._reqOPts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      /* observe: 'response',
      responseType: 'json', */
    };
   }

get(endpoint: string, params?: any) {
  if (params) {
    this._reqOPts.params = new HttpParams();
   for (const k in params) {
     if (k) {
      this._reqOPts.params.set(k, params[k]);
     }
   }
  }
  return this.http.get(this._url + '/' + endpoint , this._reqOPts).toPromise();
}

post(endpoint: string, body = null) {
return this.http.post(this._url + '/'  + endpoint, this.initApiRequest(body), this._reqOPts).toPromise();
}

initApiRequest(abody: any) {
  const tken = this.data.getData('_account').token;
 const reqBody = {
    token: tken || '',
    request: abody
  };

  return JSON.stringify(reqBody);
}

setApiURl(aUrl: string) {
  this._url = aUrl;
}

}

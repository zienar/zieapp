import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { LoggerService } from './logger.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  static ERRORS = {
    KEY_NOT_FOUND: 'Storage key isn\'t defined',
    USER_DISCONNECTED: 'User isn\'t logged in',
  };

  private localData: Map<string, any>;
  private localDataStaging: Map<string, any>;

  constructor( private storage: Storage, private logger: LoggerService) {
    this.localData = new Map<string, any>();
    this.localDataStaging = new Map<string, any>();
   }

   public InitData() {
     this.loadData('_account');
   }

   public createLocalKey(aKey: string) {
    if (aKey) {
      this.localData.set(aKey, {});
    }
  }

  public createLocalStagingKey(aKey: string) {
    if (aKey) {
      this.localDataStaging.set(aKey, {});
    }
  }

  /*
   * load data from database,k-v storage
   */
  public async loadData(aKey: string) {
    try {
      switch (aKey) {
        // Accounts
        case '_account':  const vlab = (await this.storage.get(aKey)) || [];
                          this.localData.set('_account', vlab);
        break;
        // Others : user connected path
        default:
          const username = this.localData.get('_account').login;
          if (username) {
            this.createLocalKey(aKey);
            const vla = (await this.storage.get(username + aKey)) || [];
            this.localData.set(aKey, vla);
          } else {
            // User not connected
            throw new Error(DataService.ERRORS['USER_DISCONNECTED']);
          }
          break;
      }
    } catch (error) {
      throw error;
    }
  }

  /*get cached data*/
  public getData(aKey: string) {
    let result: any;
    if (aKey) {
        result = this.localData.get(aKey);
       }
    return result;
  }

  public async setData(aKey: string, aData: any) {

    if (aKey && aData) {

        if (!this.localData.get(aKey)) {
          this.createLocalKey(aKey);
        }
          this.localData.set(aKey, aData);
          await this.saveData(aKey);
    }
  }

  public async saveData(aKey: string) {
    if (aKey) {
      try {
        const username = this.localData.get('_account').login;
        if (username) {
          await this.storage.set(username + aKey, this.localData.get(aKey));
        } else {
          // User not connected
          throw new Error(DataService.ERRORS['USER_DISCONNECTED']);
        }

      } catch (error) {
        throw error;
      }
    }
  }

  /*remove data from database*/
  public async deleteData(aKey: string) {
    if (aKey) {
      try {
        const username = this.localData.get('_account').login;
        if (username) {
          await this.storage.remove(username + aKey);
        } else {
          // User not connected
          throw new Error(DataService.ERRORS['USER_DISCONNECTED']);
        }

      } catch (error) {
        throw error;
      }
    }
  }

  /*remove from cache*/
  public deleteLocalData(akey: string) {
    if (akey) {
      this.localData.delete(akey);
    }
  }

  public getStaging(aKey: string) {
    let result: any;
    if (aKey) {

      if (this.localDataStaging.has(aKey)) {
        result = this.localDataStaging.get(aKey);
      } else {
        this.loadStaging(aKey);
        result = this.localDataStaging.get(aKey);
      }
    }
    return result;
  }

  public async loadStaging(aKey: string) {
    if (aKey) {
      try {
        const username = this.localData.get('_account').login;
        if (username) {
          const vla = (await this.storage.get(username + 'Staging' + aKey)) || [];
          this.localDataStaging.set(aKey, vla);
        } else {
          // User not connected
          throw new Error(DataService.ERRORS['USER_DISCONNECTED']);
        }

      } catch (error) {
        throw error;
      }
    }

  }

  public async setStaging(aKey: string, data: any) {
    if (aKey && data) {
      if (aKey !== '_account') {

        if (!this.localDataStaging.has(aKey)) {
          this.createLocalStagingKey(aKey);
        }
        this.localDataStaging.set(aKey, data);
        await this.saveStaging(aKey);
      } else {
        throw new Error(DataService.ERRORS['MOD_SYS_DATA']);
      }

    }
  }

  public async saveStaging(aKey: string) {
    if (aKey) {
      try {
        const username = this.localData.get('_account').login;
        if (username) {
          await this.storage.set(username + 'Staging' + aKey, this.localDataStaging.get(aKey));
        } else {
          // User not connected
          throw new Error(DataService.ERRORS['USER_DISCONNECTED']);
        }

      } catch (error) {
        throw error;
      }
    }

  }

  public async deleteStaging(aKey: string) {

    if (aKey && this.localDataStaging.has(aKey)) {
      this.localDataStaging.delete(aKey);

      try {
        const username = this.localData.get('_account').login;

        if (username) {
          await this.storage.remove(username + 'Staging' + aKey);

        } else {
          // User not connected
          throw new Error(DataService.ERRORS['USER_DISCONNECTED']);
        }

      } catch (error) {
        throw error;
      }
    }
  }


}

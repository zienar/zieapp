import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {LoggerService} from './logger.service';
import { DataService } from './data.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private api: ApiService, private log: LoggerService, private data: DataService) { }

 async createAccount(user: string, mkey: string) {
   const request = {
     Zid: user,
     key: mkey
   };

   this.log.logInfo('----------------------------------');
   this.log.logInfo('Account Being Created For: ' + user);

    // response is a json object
   await this.api.post('auth/signup', request).then((response) => {
     const json = JSON.stringify(response);
     const tkn =  JSON.parse(json).token;
     // console.log(tkn);
       this.data.setData('_account', {username: user, login: user, token: tkn});
   });

 }


 async loginAccount(userid: string, mkey: string) {

   const request = {
    Zid: userid,
    key: mkey
  };

  this.log.logInfo('----------------------------------');
  this.log.logInfo('Account Sign-in: ' + userid);

  await this.api.post('auth/login', request).then((response) => {
    const json = JSON.stringify(response);
    const tkn =  JSON.parse(json).token;
    // console.log(tkn);
      this.data.setData('_account', {username: userid, login: userid, token: tkn});
  });

 }

 isAuthenticated(): boolean {
   const acct = this.data.getData('_account').token;
   return (acct && true) || false; // return true if the account exists or false
 }

}

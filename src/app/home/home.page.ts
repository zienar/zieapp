import { Component } from '@angular/core';
import { ActionCardComponent } from '../action-card/action-card.component';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  events: Array<any>;

  constructor() {
    this.events = new Array<any>();
    this.events.push({
      title: 'Christmas Dinner',
      description: 'Please come over',
      time: '00:00',
      location: '浦东新区',
      status: 'Maybe',
      month: 'Dec',
      date: '20',
      days: '05',
      poster: '../../../src/assets/christmas.svg'
    });
  }

  goToDetails(index: number) {

  }

}
